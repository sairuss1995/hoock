import React, { Component } from 'react'

class Form extends Component {
    state = {
        password: '',
        login: '',
        error: ''
    }

    submitForm = event => {
        event.preventDefault();
        const { login, password } = this.state;

        this.setState({ error: '' });
        if (login.length < 4) {
            this.setState({ error: 'Login must contain 4 character an more!' })
        }

        if (password.length < 4) {
            this.setState(prevState => {
                let error = 'Password must contain 4 character an more!';
                if (prevState.error) {
                    error = prevState.error + '; ' + error;
                }
                return {
                    error
                }
            })
        }

        if (login && password) {
            this.props.setAuthUser({ login });
            this.setState({ password: '', login: '' })
        }
    }

    setInput = ({ target: { name, value } }) =>
        this.setState({ [name]: value })

    render() {
        const { login, password, error } = this.state;

        return (
            <div className="loginForm">
                <form onSubmit={this.submitForm}>
                    <input
                        type="text"
                        placeholder="Login"
                        name="login"
                        onChange={this.setInput}
                        value={login} />
                    <input
                        type="password"
                        placeholder="Password"
                        name="password"
                        onChange={this.setInput}
                        value={password} />
                    <button>Send</button>
                </form>
                {error && <div className="error">
                    {error}
                </div>}
            </div>
        )
    }
}
export default Form