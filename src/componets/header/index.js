import React from 'react'

export default function Header() {
    return (
        <div>
            <ul>
                <li>
                    <a href="/about" onClick={e => {
                        e.preventDefault()
                        console.log(e.target.getAttribute('href'))
                    }}>About</a>
                </li>
                <li>
                    Second
                </li>
                <li>
                    Third
                </li>
            </ul>
        </div>
    )
}
