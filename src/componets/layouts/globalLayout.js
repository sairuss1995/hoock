import React from 'react'
import Form from '../form'
import AuthLayout from './authLayout'
import EmptyLayout from './emptyLayout'

export default ({ authUser, setAuthUser }) =>
    <div className="globalLayout">

        {!authUser && <EmptyLayout>
            <Form setAuthUser={setAuthUser} />
        </EmptyLayout>}

        {authUser && <AuthLayout>
            <h2>Hello {authUser.login}</h2>
            <button onClick={() => setAuthUser(null)}>Logout</button>
        </AuthLayout>}

    </div>
