import React from 'react'
import Header from '../header'

export default ({ children }) =>
    <div className="authLayout">
        <Header />
        {children}
    </div>
