import React from 'react'

export default ({ children }) =>
    <div className="emptyLayout">
        {children}
    </div>
