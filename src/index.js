import React from 'react';
import ReactDOM from 'react-dom';
import MyApp from './App';
// import MyApp from './Router'
import './style.css';

ReactDOM.render(
  <MyApp />,
  document.querySelector('#root')
);