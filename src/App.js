import React, { Component } from 'react'
import Global from './componets/layouts/globalLayout';

export default class App extends Component {
  state = {
    authUser: null
  }

  setAuthUser = authUser => this.setState({ authUser })

  render() {
    const { authUser } = this.state;
    const { setAuthUser } = this;

    return (
      <Global authUser={authUser} setAuthUser={setAuthUser} />
    )
  }
}